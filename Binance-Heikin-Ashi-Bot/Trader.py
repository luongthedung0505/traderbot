import time, datetime
import math
from binance.client import Client
import pandas as pd
import numpy as np
import sys
import configparser
from numpy.lib import NumpyVersion
import websocket, json, pprint, talib, numpy
from binance.client import Client
from binance.enums import *

EMA_10 = 10 
EMA_20 = 20
ema10lst = []
ema20lst = []
has_no_upper_wick = False
has_no_lower_wick = False
normalStick = False
blueStick = False 
RedStick = False 
Uptrend = False 
Downtrend = False  
delta_fast = 0.2
priceSellPosition = 0
priceBuyPosition = 0  
SellPosition = 0 
BuyPosition = 0
signal = False
white_body = False
Bought = False
Sold = False
class Trader(Client):
    def __init__(self, api_key, api_secret, live_trading):
        super(Trader, self).__init__(api_key, api_secret)
        self.live_trading = live_trading
        self.time_offset = self.get_time_offset()
        self.exchange_info = self.get_exchange_info()

    def _request(self, method, uri, signed, force_params=False, **kwargs):
        ### **Developer's note** This function has to be overwritten because it is causing errors when there is a time delay between 
        # the binance server and the local server

        # set default requests timeout
        kwargs['timeout'] = 10

        # add our global requests params
        if self._requests_params:
            kwargs.update(self._requests_params)

        data = kwargs.get('data', None)
        if data and isinstance(data, dict):
            kwargs['data'] = data

            # find any requests params passed and apply them
            if 'requests_params' in kwargs['data']:
                # merge requests params into kwargs
                kwargs.update(kwargs['data']['requests_params'])
                del(kwargs['data']['requests_params'])

        if signed:
            # generate signature
            kwargs['data']['timestamp'] = int(time.time() * 1000 + self.time_offset)
            kwargs['data']['signature'] = self._generate_signature(kwargs['data'])

        # sort get and post params to match signature order
        if data:
            # sort post params
            kwargs['data'] = self._order_params(kwargs['data'])
            # Remove any arguments with values of None.
            null_args = [i for i, (key, value) in enumerate(kwargs['data']) if value is None]
            for i in reversed(null_args):
                del kwargs['data'][i]

        # if get request assign data array to params value for requests lib
        if data and (method == 'get' or force_params):
            kwargs['params'] = '&'.join('%s=%s' % (data[0], data[1]) for data in kwargs['data'])
            del(kwargs['data'])

        self.response = getattr(self.session, method)(uri, **kwargs)
        return self._handle_response()

    def get_time_offset(self):
        # Getting the time difference between the binance server time and the local server
        st = int(self.get_server_time()["serverTime"])
        mt = int(time.time()*1000)
        return st - mt

    def get_precision(self, symbol, type='stepSize'):
        infos = self.exchange_info["symbols"]
        for info in infos:
            if info["baseAsset"] == symbol[:len(info["baseAsset"])]:
                data = info
                break
        if type == "stepSize":
            tick = data["filters"][2]["stepSize"]
            precision = math.floor(-math.log10(float(tick)))
            return precision
        if type == "tickSize":
            tick = data["filters"][0]["tickSize"]
            precision = math.floor(-math.log10(float(tick)))
            return precision
        elif type == "base":
            return data["baseAssetPrecision"]
        elif type == "quote":
            return data["quotePrecision"]

    def place_market_buy(self, symbol, quantity):
        data = self.order_market_buy(symbol=symbol, quantity=quantity)
        return data

    def place_market_sell(self, symbol, quantity):
        data = self.order_market_sell(symbol=symbol, quantity=quantity)
        return data

    def get_balance(self, asset):
        balance = float(self.get_asset_balance(asset)['free'])
        return balance

    def round_down_safely(self, x, precision):
        # rounds down safely by avoiding floating point errors
         return round(math.floor(x * (10**precision)) * 10**-precision, precision)

    def place_market_order(self, base_asset="BTC", quote_asset="USDT", action="sell", percentage=100):
        # Get base asset and quote asset
        symbol = base_asset + quote_asset

        # Determine transaction quantity
        precision = self.get_precision(symbol, "stepSize")
        
        if action == 'sell':
            asset = base_asset
        elif action == 'buy':
            asset = quote_asset
        balance = self.get_balance(asset)

        if action == 'sell':
            quantity = self.round_down_safely(balance * percentage / 100, precision)
        elif action == 'buy':
            last_price = float(self.get_symbol_ticker(symbol=symbol)['price'])
            print("last price", last_price)
            quantity = self.round_down_safely(balance * percentage / 100 / last_price, precision)

        print("Balance = {}, Percentage = {}, Precision = {}, Quantity = {}".format(balance, percentage, precision, quantity))
        assert quantity > 0.0, "Balance of {} {} is too low to execute trade.".format(balance, asset)

        # Execute trade
        if self.live_trading:
            if action == 'sell':
                response = self.place_market_sell(symbol, quantity)
            elif action == 'buy':
                response = self.place_market_buy(symbol, quantity)
            else:
                raise NotImplementedError('Action {} is not implemented.'.format(action))
        else:
            response = {'status': 'Live trading is off'}
        
        # Log
        msg = "Placing {} order for {} of {}".format(action, quantity, symbol)
        print(msg)
        print(response)
        return response['status'] == 'FILLED'


    ### Related to trading strategy ###
    # 1 - Get kline data for past 1000 candles
    # 2 - Build Heikin-Ash candlesticks and colors
    # 3 - Make trade

    # Other points
    # - Start out in a state (which pair, current position)
    # - Maintain the state (if in buy or sell)
    # - Log trades
	# - If there are any errors, stop trading to prevent unexpected behavior

        

        
    def start_bot(self, base_asset, quote_asset, initial_position, interval='2h', wait_time=60):
        '''EXAMPLE:
        base_asset = 'BTC'
        quote_asset = 'PERP'
        initial_position = 'short' // Can be 'short' or 'long' and tells the bot where you are starting off
        interval = '1h' // Can be any of {1m,3m,5m,15m,30m,1h,2h,4h,6h,8h,12h,1d,3d,1w,1M}
        wait_time = 60 // Seconds between API queries
        '''
        global ema10lst,ema20lst,Uptrend,Downtrend,has_no_lower_wick,has_no_upper_wick,RedStick,blueStick, normalStick ,priceSellPosition,priceBuyPosition, SellPosition, BuyPosition,signal
        global white_body ,Bought,Sold
        print("Bot Starting at {}".format(datetime.datetime.today()))
        
        print("Base Asset: {} Quote Asset: {} Interval: {} Wait Time: {}".format(base_asset, quote_asset, interval, wait_time))
        position = initial_position
        print("Initial Position: {}".format(position))
        symbol = base_asset + quote_asset
        while True:
            ## Get data from API
            num_rows = 1000
            candles = t.get_klines(symbol=symbol, interval=interval, limit=num_rows)
            df = pd.DataFrame(candles, columns=['open_time', 'open', 'high', 'low', 'close', 'volume', 'close_time', 'quote_asset_volume', 'number_of_trades', 'taker_buy_base_asset_volume', 'taker_buy_quote_asset_volume', 'ignore'])
            num_rows = df.shape[0] # Make sure that num_rows is correct in case 1000 periods haven't happened yet

            ## Clean data
            df = df[['open_time', 'open', 'high', 'low', 'close', 'close_time']]
            df['open'] = df['open'].astype('float')
            df['close'] = df['close'].astype('float')
            df['high'] = df['high'].astype('float')
            df['low'] = df['low'].astype('float')

            ## Calculate Heikin-Ashi values (source: https://school.stockcharts.com/doku.php?id=chart_analysis:heikin_ashi)
            # HA Close is based on current open close and low
            df['HA_close'] = (df['open'] + df['close'] + df['low'] + df['close']) / 4
            # Set HA values to regular open and close for first period in data
            df.at[0, 'HA_open'] = df['open'].iloc[0]
            df.at[0, 'HA_close'] = df['close'].iloc[0]
            # Set HA_open with recursive formula
            for i in range(1, num_rows):
                df.at[i, 'HA_open'] = (df.loc[i-1, 'HA_open'] + df.loc[i-1, 'HA_close']) / 2
            # Set HA_high and HA_low
            df['HA_high'] = df[['high', 'HA_open', 'HA_close']].max(axis=1)
            df['HA_low'] = df[['low', 'HA_open', 'HA_close']].min(axis=1)
            # Set color of HA candle (True = Green) if HA_close=HA_open, take previous color
            for i in range(0, df.shape[0]):
                d = df.at[i, 'HA_close'] > df.at[i, 'HA_open']

                if i > 0 and df.at[i, 'HA_close'] == df.at[i, 'HA_open']:
                    d = df.at[i-1, 'HA_green_candle']
                df.at[i, 'HA_green_candle'] = d
            # print("EMA list")
            ema10lst = df[989:999]['HA_close']
            ema20lst = df[979:999]['HA_close']
            wma45lst = df[954:999]['HA_close']
            rsi14lst = df[985:999]['HA_close']
            
            highNow =float(df [998:999]['HA_high']) 
            lowNow = float(df [998:999]['HA_low'])
            openNow = float( df [998:999]['HA_open'])
            closeNow = float( df [998:999]['HA_close'])
#             white_body = ha_close > ha_open
# black_body = ha_close <= ha_open
            if closeNow > openNow :  
                white_body = True
            else :
                white_body = False
            
            
            # colorCandle = df [998:999]['HA_green_candle']
            if lowNow == openNow or lowNow == closeNow :
                has_no_lower_wick = True
                has_no_upper_wick = False
                normalStick = False
            else :
                 if highNow == openNow or highNow == closeNow :
                    normalStick = False 
                    has_no_lower_wick = False
                    has_no_upper_wick = True
                 else :
                    normalStick = True 
                    has_no_lower_wick = False
                    has_no_upper_wick = False

            # print(ema10lst)
            np_closes = numpy.array(ema10lst)
            ema10Value  = talib.EMA(np_closes,10)
            np_closes20 = numpy.array  (ema20lst)
            ema20Value = talib.EMA (np_closes20,20)
            # wma45Value = numpy.array (wma45lst,45) 
            # rsiValue = numpy.array (rsi14lst,14)
            # print( ema10Value)
            # print(ema20Value)
            distance = float(ema10Value[9]) - float(ema20Value[19])
            # print('Gia tri cua ema10Value {}', ema10Value[9])
            # print('Gia tri cua ema20Value {}', ema20Value[19])
            # print('Gia tri cua distance {}', distance)
            # print('Gia tri cua distanct {}', distanct)
            if distance > 0:
                Uptrend = True  
                Downtrend = False
            else :
                if distance < 0 :
                    Uptrend = False
                    Downtrend = True
                    
                else :
                    if Uptrend :
                        print(format(datetime.datetime.today()))
                        print ('Diem mua tai day : {}',current_price)
                        print(distance)
                        print( float(ema10Value[9]))
                    else :
                       print(format(datetime.datetime.today()))
                       print ('Diem ban tai day : {}',current_price)
                       print(distance)
                       print( float(ema10Value[9]))
          
            print('Gia tri cua Uptrend {}', Uptrend)
            ## Execute trading logic Long And Short Entry 
        
            stoploss = 0 
            takeprofit = 0  
            last_candle = df['HA_green_candle'].iloc[-2]
            second_to_last_candle = df['HA_green_candle'].iloc[-3]
            current_price = df['close'].iloc[-1]
            stoploss = df['close'].iloc[-2]
            # print('Gia tri cua last_Candle {} and second_to_last {}', last_candle,second_to_last_candle)
            
            if last_candle == second_to_last_candle:
                signal = 'none'
                # print(signal) 
                # print(last_candle)
                # print(Uptrend)
            # elif distanct < 5 and distanct >0 :
            #     signal = 'buy'
            #     # stoploss = df['close'].iloc[-2] 
               
            #     #need to create tp/sl ( sl : stoplost = )
            # elif  distanct > -5 and distanct < 0:
            #     signal = 'sell'
                # stoploss = df['close'].iloc[-2]
             ## Execute Exist Long And Short 
#              long_exit = uptrend and (black_body and has_no_upper_wick)
                # short_exit = downtrend and (white_body and has_no_lower_wick)
                
            # print ('Gia tri has no_uper :{} and has no lower :{}',has_no_upper_wick,has_no_lower_wick)
            if Uptrend and (white_body == False and last_candle ==True and has_no_upper_wick) :
                print(format(datetime.datetime.today()))
                print('Long Exist ')
                print(closeNow)
                # signal = 'sale'
                # print ('Take profit here')
                #create stop vị thế 
            if  Downtrend and (white_body == True and  last_candle == False and has_no_lower_wick) :
                print(format(datetime.datetime.today()))
                print ('Short Exist')
                print(closeNow)
                # signal = 'buy'
                # print ('Take profit here')
                #create stop vị thế 
            print('Distance : {}',distance)
            if distance < 3 and distance >0 :
                signal = 'buy'
                stoploss = df['close'].iloc[-2] 
               
                #need to create tp/sl ( sl : stoplost = )
            elif  distance > -3 and distance < 0:
                signal = 'sell'
                stoploss = df['close'].iloc[-2] 

            ## Make trade
            # print("{}\tSignal: {}\tPosition: {}\tCurrent Price: {}".format(datetime.datetime.today(), signal, position, current_price))
            if signal != 'none':
                if  signal == 'buy':
                    if Bought == False :
                        print("{}\tSignal: {}\tPosition: {}\tCurrent Price: {}\t SL : {}".format(datetime.datetime.today(), signal, position, current_price,stoploss))
                        Bought = True
                        Sold = False
                    # self.place_market_order(base_asset=base_asset, quote_asset=quote_asset, action='buy')
                    # position = 'in_trade'
                elif signal == 'sell':
                    if Sold == False :
                       print("{}\tSignal: {}\tPosition: {}\tCurrent Price: {}\t SL :{}".format(datetime.datetime.today(), signal, position, current_price,stoploss))
                       Sold = True
                       Bought = False
                    # self.place_market_order(base_asset=base_asset, quote_asset=quote_asset, action='sell')
                    # position = 'not_in_trade'
            
            # Wait

#   
            # delta_value = float(ema10Value[9]) - float(ema20Value[19])
            # if delta_value < delta_fast or (Uptrend and has_no_lower_wick) :
            #         if BuyPosition ==0 :
            #             priceBuyPosition = closeNow 
            #             print('diem mua at {}',closeNow)
            #             BuyPosition = 0 
            #         else :
            #             print('Đã mua ở {}',priceBuyPosition)
            #             BuyPosition = 0 
            # if delta_value > 0 or (Downtrend and has_no_upper_wick):
            #          if SellPosition == 0 :
            #             priceSellPosition = closeNow 
            #             print('diem bán  at {}',closeNow)
            #             SellPosition = 0
            #          else :
            #             print('Đã bán ở {}',priceSellPosition)
            #             SellPosition = 0

            time.sleep(wait_time)

if __name__ == '__main__':
    ## Get configuration data
    config_fname = sys.argv[1]
    config = configparser.ConfigParser()
    config.sections()
    config.read(config_fname)
    assert config['TRADING']['LIVE_TRADING'] in ['True', 'False'], 'Configuration value for LIVE_TRADING ({}) is invalid. Must be True or False.'.format(config['TRADING']['LIVE_TRADING'])
    assert config['TRADING']['INITIAL_POSITION'] in ['in_trade', 'not_in_trade'], 'Configuration value for INITIAL_POSITION ({}) is invalid. Must be in_trade or not_in_trade.'.format(config['TRADING']['INITIAL_POSITION'])

    ## Start trading
    t = Trader(config['API_KEYS']['API_KEY'], 
                config['API_KEYS']['API_SECRET'], 
                live_trading=config['TRADING']['LIVE_TRADING']=='True'
                )

    t.start_bot(config['TRADING']['BASE_ASSET'], 
                config['TRADING']['QUOTE_ASSET'],
                config['TRADING']['INITIAL_POSITION'],
                interval=config['TRADING']['INTERVAL'],
                wait_time=int(config['TRADING']['WAIT_TIME']),
                )
